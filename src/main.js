import {
	createSSRApp
} from "vue";
import App from "./App.vue";
// 导入搜索组件
import Search from '@/components/search'
// 注册插件
import { RequestPlug } from '@/utils/request'
// 导入mixin
import MyMixin from '@/utils/mixin'
export function createApp () {
	const app = createSSRApp(App);
	// 注册全局组件或方法
	// 语法：app.component('组件的元素名', 导入组件对象)
	app.component('Search', Search)
	// 注册插件
	app.use(RequestPlug)
	// 注册mixin=>app.mixin方法
	app.mixin(MyMixin)
	return {
		app,
	};
}
