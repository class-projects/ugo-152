/**
 * 全局混入鉴权（路由拦截）方法
 */
export default {
  data () {
    return {
      globalDatas: '我是mixin混入的数据'
    }
  },
  methods: {
    checkAuth () {
      // 根据是否有token
      if (!uni.getStorageSync('ugo-token')) {
        // 没有登录，跳转到登录页
        uni.navigateTo({
          url: '/packone/auth/index'
        })
      }
    }
  }
}