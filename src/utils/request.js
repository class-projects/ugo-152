/**
 * request 函数=》发请求
* 封装思路：参数=》{ url, method, header = {}, data }
* 1. 发送请求之前：显示loading，统一添加token
* 2. 请求中：== 使用uni.request()执行请求发送 ==
* 3. 发请求之后：判断请求是否成功？res.statusCode >= 200 && res.statusCode < 300
*    1. 如果成功，返回后台数据
*    2. 如果失败，抛出promise错误
*/
// 请求基础地址
const BASE_URL = 'https://api-hmugo-web.itheima.net'
/**
 * 技术点：
 * 一个函数加上async关键字，执行完返回Promise对象(语法糖)
 */
/**
 * header = {} =》没有header属性，给空对象默认值
 * @param {*} object: { url, method, header = {}, data }
 */
const request = async ({ url, method, header = {}, data }) => {
  // 1. 发请求前准备
  uni.showLoading({
    title: 'loading...', // 请求开始的提示
    mask: true // 生成一个遮罩层，盖住整个页面，避免没有数据时，用户点击页面
  })

  // 从本地获取token=》未来开发登录，会把token存储到本地
  const token = uni.getStorageSync('ugo-token')
  if (token) {
    header.Authorization = token
  }
  // 2. 发请求了
  try {
    const res = await uni.request({
      url: BASE_URL + url, // 拼接请求地址=》使用request方法只用传递当前接口路径
      method,
      header,
      data
    })
    if (res.statusCode >= 200 && res.statusCode < 300) {
      // 返回数据=》外边调用request函数，可以通过await获取
      return {
        msg: res.data.meta, // 后台返回接口消息状态
        data: res.data.message // 返回数据
      }
    }
  } catch (error) {
    // try块代码执行失败走这里
    return Promise.reject(error)
  } finally {
    // try块代码执行成功/失败都走这里
    uni.hideLoading()
  }

  // return new Promise(callback)
  // return 值 === resolve(123)
  // return 123
}

const request2 = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(456)
    }, 2000)
  })

}

// 封装一个vue插件
const RequestPlug = {
  // vue3:app根实例 => ｜ vue2:Vue构造函数=>Vue.prototype.fn=fn
  install (app) {
    // uni-app支持在插件中注册全局方法
    app.config.globalProperties.request = request
  }
}

export { request, request2, RequestPlug }